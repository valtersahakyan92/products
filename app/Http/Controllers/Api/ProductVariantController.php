<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductVariantRequest;
use App\Http\Requests\UpdateProductVariantRequest;
use App\Http\Resources\ProductVariantResource;
use App\Models\ProductVariant;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ProductVariantController extends Controller
{
    /**
     * @OA\Get(
     *      path="/product/variants",
     *      operationId="getProductVariantsList",
     *      tags={"ProductVariants"},
     *      summary="Get list of product variants",
     *      description="Returns list of product variants",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductVariantResource")
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return ProductVariantResource::collection(ProductVariant::with(['products'])->get());
    }

    /**
     * @OA\Post(
     *      path="/product/variants",
     *      operationId="storeProductVariant",
     *      tags={"ProductVariants"},
     *      summary="Store new product",
     *      description="Returns product data",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/StoreProductVariantRequest")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductVariant")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreProductVariantRequest $request): \Illuminate\Http\JsonResponse
    {
        $product = ProductVariant::create($request->all());

        return (new ProductVariantResource($product))
            ->response()
            ->setStatusCode(ResponseAlias::HTTP_CREATED);
    }


    /**
     * @OA\Get(
     *      path="/product/variants/{id}",
     *      operationId="getProductVariantById",
     *      tags={"ProductVariants"},
     *      summary="Get product information",
     *      description="Returns product data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Product id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductVariant")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function show(ProductVariant $product, $id)
    {
        return new ProductVariantResource($product->load(['products'])->findOrFail($id));
    }

    /**
     * @OA\Put(
     *      path="/product/variants/{id}",
     *      operationId="updateProductVariant",
     *      tags={"ProductVariants"},
     *      summary="Update existing product variant",
     *      description="Returns updated product variant data",
     *      @OA\Parameter(
     *          name="id",
     *          description="Product id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/UpdateProductVariantRequest")
     *      ),
     *      @OA\Response(
     *          response=202,
     *          description="Successful operation",
     *          @OA\JsonContent(ref="#/components/schemas/ProductVariant")
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function update(UpdateProductVariantRequest $request, ProductVariant $product, $id): \Illuminate\Http\JsonResponse
    {
        $product->findOrFail($id)->update($request->all());
        return (new ProductVariantResource($product->findOrFail($id)))
            ->response()
            ->setStatusCode(ResponseAlias::HTTP_ACCEPTED);
    }

    /**
     * @OA\Delete(
     *      path="/product/variants/{id}",
     *      operationId="deleteProductVariant",
     *      tags={"ProductVariants"},
     *      summary="Delete existing variant",
     *      description="Deletes a record and returns no content",
     *      @OA\Parameter(
     *          name="id",
     *          description="Product variant id",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=204,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */
    public function destroy(ProductVariant $product): Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        $product->delete();

        return response(null, ResponseAlias::HTTP_NO_CONTENT);
    }
}
