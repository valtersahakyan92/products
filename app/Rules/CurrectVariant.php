<?php

namespace App\Rules;

use App\Models\ProductVariant;
use Illuminate\Contracts\Validation\InvokableRule;

class CurrectVariant implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        if (!ProductVariant::find($value)) {
            $fail('The :attribute must be exists.');
        }
    }
}
