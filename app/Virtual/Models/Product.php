<?php
namespace App\Virtual\Models;
/**
* @OA\Schema(
*     title="Project",
*     description="Project model",
*     @OA\Xml(
*         name="Product"
*     )
* )
*/
class Product
{

/**
* @OA\Property(
*     title="ID",
*     description="ID",
*     format="int64",
*     example=1
* )
*
* @var integer
*/
private $id;

/**
* @OA\Property(
*      title="Name",
*      description="Name of the new product",
*      example="A nice product"
* )
*
* @var string
*/
public $name;

/**
* @OA\Property(
*      title="Description",
*      description="Description of the new roduct",
*      example="This is new product's description"
* )
*
* @var string
*/
public $description;

/**
* @OA\Property(
*     title="Created at",
*     description="Created at",
*     example="2020-01-27 17:50:45",
*     format="datetime",
*     type="string"
* )
*
* @var \DateTime
*/
private $created_at;

/**
* @OA\Property(
*     title="Updated at",
*     description="Updated at",
*     example="2020-01-27 17:50:45",
*     format="datetime",
*     type="string"
* )
*
* @var \DateTime
*/
private $updated_at;


/**
* @OA\Property(
*      title="Variant ID",
*      description="Variant's id of the new product",
*      format="int64",
*      example=1
* )
*
* @var integer
*/
public $variant_id;


/**
* @OA\Property(
*     title="Variant Product",
*     description="Product variants's variantProduct model"
* )
*
* @var ProductVariant
*/
private $variant;
}
