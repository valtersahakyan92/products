<?php
namespace App\Virtual\Resources;
use App\Virtual\Models\ProductVariant;

/**
 * @OA\Schema(
 *     title="ProductVariane Resource",
 *     description="Product resource",
 *     @OA\Xml(
 *         name="ProductVarianeResource"
 *     )
 * )
 */
class ProductVariantResource
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var ProductVariant[]
     */
    private $data;
}
