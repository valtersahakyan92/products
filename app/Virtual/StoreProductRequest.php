<?php
namespace App\Virtual;
/**
* @OA\Schema(
*      title="Store Product request",
*      description="Store Product request body data",
*      type="object",
*      required={"name", "description", "variant_id"}
* )
*/

class StoreProductRequest
{
/**
* @OA\Property(
*      title="name",
*      description="Name of the new product",
*      example="A nice product"
* )
*
* @var string
*/
public $name;

/**
* @OA\Property(
*      title="description",
*      description="Description of the new product",
*      example="This is new product's description"
* )
*
* @var string
*/
public $description;

/**
* @OA\Property(
*      title="variant_id",
*      description="Product variant's id of the new product",
*      format="int64",
*      example=1
* )
*
* @var integer
*/
public $variant_id;
}
