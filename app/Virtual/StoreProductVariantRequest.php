<?php
namespace App\Virtual;
/**
* @OA\Schema(
*      title="Store Product request",
*      description="Store Product request body data",
*      type="object",
*      required={"name", "description", "variant_id"}
* )
*/

class StoreProductVariantRequest
{
/**
* @OA\Property(
*      title="name",
*      description="Name of the new product",
*      example="A nice product"
* )
*
* @var string
*/
public $name;
}
